# Botza

Bots Analytics utility application.
Receives messages from a chatbot (currently only Watson V1) and provides some
insights into the conversations.

This is currently a hobby project to get some insight into the data generated
by Watson & Co and experiment with Nest.js in the process.

## Deploying

There's a `Procfile` already in the repository, so all you need to do to deploy
this on a service like Heroku is to provide a `DATABASE_URL` environment
variable and provision the database with a schema.
Migrations and further instructions will follow soon.

Then, create an entry in the `bot` table and hook up your assistants message
output to the URL `$host/watson/v1/$botId/messages`.
Now, any messages the application receives will be destructured into the
relational database and can be queried with a BI tool like Metabase.

## Contributing

Bug reports and merge requests are welcome at:
https://gitlab.com/milgner/botza/
