import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {WatsonV1Module} from "./watson/v1/module";
import {ConfigModule, ConfigService} from "nestjs-config";
import * as path from "path";

const ENV = process.env.NODE_ENV;

@Module({
  imports: [
    ConfigModule.load(path.resolve(__dirname, 'config/!(*.d).{ts,js}'), {
      path: path.resolve(process.cwd(), !ENV ? '.env' : `.env.${ENV}`),
    }),
    TypeOrmModule.forRootAsync({
      useFactory: function(config: ConfigService) {
        return config.get('typeorm');
      },
      inject: [ConfigService]
    }),
    WatsonV1Module
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
