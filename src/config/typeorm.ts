import * as path from "path";

export default {
  "type": "postgres",
  "url": process.env.DATABASE_URL,
  "entities": [path.resolve(__dirname, "../models/*.entity{.ts,.js}")],
  "synchronize": process.env.NODE_ENV == 'test' || process.env.TYPEORM_SYNCHRONIZE,
  "uuidExtension": "pgcrypto",
}
