import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {IntentReference} from "./intent-reference.entity";

@Entity()
export class Intent {
  @PrimaryGeneratedColumn("uuid")
  id: String;

  @Column("text")
  name: String;

  @OneToMany(type => IntentReference, ref => ref.intent, {
    cascade: true
  })
  references: IntentReference[];
}