import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Bot} from "./bot.entity";
import {Message} from "./message.entity";

@Entity()
export class Conversation {
  @PrimaryGeneratedColumn("uuid")
  id: String;

  @Column("uuid")
  externalId: String;

  @ManyToOne(type => Bot, bot => bot.conversations)
  bot: Bot;

  @ManyToOne(type => Message, message => message.conversation)
  messages: Message[];
}