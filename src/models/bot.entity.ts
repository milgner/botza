import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Conversation} from "./conversation.entity";

@Entity()
export class Bot {
  @PrimaryGeneratedColumn("uuid")
  id: String;

  @Column("text")
  name: String;

  @OneToMany(type => Conversation, conversation => conversation.bot)
  conversations: Conversation[];
}