import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Message} from "./message.entity";
import {Intent} from "./intent.entity";

@Entity()
export class IntentReference {
  @PrimaryGeneratedColumn("uuid")
  id: String;

  @Column("decimal")
  confidence: Number;

  @ManyToOne(type => Message, message => message.intentReferences)
  message: Message;

  @ManyToOne(type => Intent, intent => intent.references)
  intent: Intent;
}