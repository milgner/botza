import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Conversation} from "./conversation.entity";
import {IntentReference} from "./intent-reference.entity";

@Entity()
export class Message {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column("text")
  input: string;

  @Column("text")
  output: string;

  @Column('jsonb')
  context: any;

  @Column({ type: 'timestamp', default: 'NOW()' })
  createdAt: Date;

  @ManyToOne(type => Conversation, conversation => conversation.messages)
  conversation: Conversation;

  @OneToMany(type => IntentReference, ref => ref.message)
  intentReferences: IntentReference[];
}