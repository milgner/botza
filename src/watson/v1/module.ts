import {Module} from "@nestjs/common";
import {MessagesController} from "./messages.controller";
import {MessageService} from "./message.service";
import {TypeOrmModule} from "@nestjs/typeorm";
import {Message} from "../../models/message.entity";
import {Conversation} from "../../models/conversation.entity";
import {Intent} from "../../models/intent.entity";
import {IntentReference} from "../../models/intent-reference.entity";
import {Bot} from "../../models/bot.entity";

@Module({
  controllers: [MessagesController],
  providers: [MessageService],
  exports: [],
  imports: [TypeOrmModule.forFeature([Message, Conversation, Intent, Bot, IntentReference])]
})
export class WatsonV1Module {}