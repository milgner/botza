import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";

import {Bot} from "../../models/bot.entity";

import {IntentReference} from "../../models/intent-reference.entity";
import {Intent } from "../../models/intent.entity";
import {Message} from "../../models/message.entity";
import {Conversation} from "../../models/conversation.entity";

import {MessageDTO} from "./message.dto";

@Injectable()
export class MessageService {
  constructor(
    @InjectRepository(Message)
    private readonly  messageRepository: Repository<Message>,
    @InjectRepository(Conversation)
    private readonly  conversationRepository: Repository<Conversation>,
    @InjectRepository(Intent)
    private readonly  intentRepository: Repository<Intent>,
    @InjectRepository(IntentReference) // TODO: might be better to have an auto-save association here
    private readonly  intentReferenceRepository: Repository<IntentReference>,
    @InjectRepository(Bot)
    private readonly  botRepository: Repository<Bot>,
  ) {}

  async save(botId: string, dto: MessageDTO): Promise<Message> {
    const bot = await this.botRepository.findOne(botId);
    const message = new Message();
    message.conversation = await this.findOrCreateConversation(bot, dto.context.conversation_id);
    message.context = dto.context;
    message.input = dto.input.text;
    message.output = dto.output.text.join("\n");
    message.intentReferences = await Promise.all(dto.intents.map((intent: { confidence: number, intent: string }) =>
      this.createIntentReference(bot, intent)));
    return this.messageRepository.save(message);
  }

  private async createIntentReference(bot: Bot, intent: { confidence: number, intent: string }): Promise<IntentReference> {
    const intentReference = new IntentReference();
    intentReference.intent = await this.findOrCreateIntent(bot, intent.intent);
    intentReference.confidence = intent.confidence;
    return this.intentReferenceRepository.save(intentReference);
  }

  private async findOrCreateIntent(bot: Bot, intentName: string): Promise<Intent> {
    const existing = await this.intentRepository.createQueryBuilder("intent")
      .innerJoinAndSelect("intent.references", "references")
      .innerJoinAndSelect("references.message", "messages")
      .innerJoinAndSelect("messages.conversation", "conversations")
      .innerJoinAndSelect("conversations.bot", "bot")
      .where("intent.name = :intentName")
      .andWhere("bot.id = :botId")
      .setParameters({ intentName: intentName, botId: bot.id})
      .getOne();
    if (existing) {
      return existing;
    }
    const intent = new Intent();
    intent.name = intentName;
    return this.intentRepository.save(intent);
  }

  private async findOrCreateConversation(bot: Bot, conversationId: string): Promise<Conversation> {
    let conversation = await this.conversationRepository.findOne({ externalId: conversationId });
    if (conversation) {
      return conversation;
    }
    conversation = new Conversation();
    conversation.bot = bot;
    conversation.externalId = conversationId;
    return this.conversationRepository.save(conversation);
  }
}
