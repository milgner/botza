export class MessageDTO {
  input: { text: string };
  output: {
    text: string[];
    nodes_visited: string[];
    log_messages: {
      level: string;
      msg: string;
    }[];
  };
  intents: {
    confidence: number;
    intent: string;
  }[];
  context: any;
  entities: {
    entity: string;
    location: number[];
    value: string;
    confidence: number;
    metadata: any;
    groups: {
      group: string;
      location: number[];
    }[];
    alternatives: {
      value: string;
      confidence: number;
    }[];
    role: {
      type: string;
    }
  }[];
}