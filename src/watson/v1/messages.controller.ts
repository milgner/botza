import {Body, Controller, HttpCode, Inject, Param, Post, Req} from "@nestjs/common";
import { Request } from 'express';
import {MessageService} from "./message.service";
import {MessageDTO} from "./message.dto";

@Controller('watson/v1/:botId/messages')
export class MessagesController {
  @Inject()
  messageService: MessageService;

  @Post()
  @HttpCode(204)
  create(@Param('botId') botId: string, @Body() message: MessageDTO, @Req() request: Request) {
    return this.messageService.save(botId, message);
  }
}
