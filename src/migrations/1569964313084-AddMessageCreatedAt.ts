import {MigrationInterface, QueryRunner} from "typeorm";

export class AddMessageCreatedAt1569964313084 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "message" ADD "createdAt" TIMESTAMP NOT NULL DEFAULT NOW()`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "message" DROP COLUMN "createdAt"`, undefined);
    }

}
