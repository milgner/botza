import {MigrationInterface, QueryRunner} from "typeorm";

export class SwitchToPgCrypto1570097064828 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('CREATE EXTENSION IF NOT EXISTS "pgcrypto"');
        await queryRunner.query(`ALTER TABLE "intent" ALTER COLUMN "id" SET DEFAULT 'gen_random_uuid()'`, undefined);
        await queryRunner.query(`ALTER TABLE "intent_reference" ALTER COLUMN "id" SET DEFAULT 'gen_random_uuid()'`, undefined);
        await queryRunner.query(`ALTER TABLE "message" ALTER COLUMN "id" SET DEFAULT 'gen_random_uuid()'`, undefined);
        await queryRunner.query(`ALTER TABLE "conversation" ALTER COLUMN "id" SET DEFAULT 'gen_random_uuid()'`, undefined);
        await queryRunner.query(`ALTER TABLE "bot" ALTER COLUMN "id" SET DEFAULT 'gen_random_uuid()'`, undefined);

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "intent" ALTER COLUMN "id" SET DEFAULT 'uuid_generate_v4()'`, undefined);
        await queryRunner.query(`ALTER TABLE "intent_reference" ALTER COLUMN "id" SET DEFAULT 'uuid_generate_v4()'`, undefined);
        await queryRunner.query(`ALTER TABLE "message" ALTER COLUMN "id" SET DEFAULT 'uuid_generate_v4()'`, undefined);
        await queryRunner.query(`ALTER TABLE "conversation" ALTER COLUMN "id" SET DEFAULT 'uuid_generate_v4()'`, undefined);
        await queryRunner.query(`ALTER TABLE "bot" ALTER COLUMN "id" SET DEFAULT 'uuid_generate_v4()'`, undefined);
    }

}
