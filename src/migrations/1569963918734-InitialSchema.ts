import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialSchema1569963918734 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "intent" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" text NOT NULL, CONSTRAINT "PK_513a9beef6edc5701fbd955321d" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "intent_reference" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "confidence" numeric NOT NULL, "messageId" uuid, "intentId" uuid, CONSTRAINT "PK_542c30e9a386ae179c5158e079c" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "message" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "input" text NOT NULL, "output" text NOT NULL, "context" jsonb NOT NULL, "conversationId" uuid, CONSTRAINT "PK_ba01f0a3e0123651915008bc578" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "conversation" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "externalId" uuid NOT NULL, "botId" uuid, "messagesId" uuid, CONSTRAINT "PK_864528ec4274360a40f66c29845" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "bot" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" text NOT NULL, CONSTRAINT "PK_bc6d59d7870eb2efd5f7f61e5ca" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`ALTER TABLE "intent_reference" ADD CONSTRAINT "FK_07631312e386cb0809ba80480ee" FOREIGN KEY ("messageId") REFERENCES "message"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "intent_reference" ADD CONSTRAINT "FK_6dc6e89562fec321286fec59d7e" FOREIGN KEY ("intentId") REFERENCES "intent"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "message" ADD CONSTRAINT "FK_7cf4a4df1f2627f72bf6231635f" FOREIGN KEY ("conversationId") REFERENCES "conversation"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "conversation" ADD CONSTRAINT "FK_34f0c9e33195e363cd90cef5a19" FOREIGN KEY ("botId") REFERENCES "bot"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "conversation" ADD CONSTRAINT "FK_ec7506a64b2227c0a89e3335d76" FOREIGN KEY ("messagesId") REFERENCES "message"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "conversation" DROP CONSTRAINT "FK_ec7506a64b2227c0a89e3335d76"`, undefined);
        await queryRunner.query(`ALTER TABLE "conversation" DROP CONSTRAINT "FK_34f0c9e33195e363cd90cef5a19"`, undefined);
        await queryRunner.query(`ALTER TABLE "message" DROP CONSTRAINT "FK_7cf4a4df1f2627f72bf6231635f"`, undefined);
        await queryRunner.query(`ALTER TABLE "intent_reference" DROP CONSTRAINT "FK_6dc6e89562fec321286fec59d7e"`, undefined);
        await queryRunner.query(`ALTER TABLE "intent_reference" DROP CONSTRAINT "FK_07631312e386cb0809ba80480ee"`, undefined);
        await queryRunner.query(`DROP TABLE "bot"`, undefined);
        await queryRunner.query(`DROP TABLE "conversation"`, undefined);
        await queryRunner.query(`DROP TABLE "message"`, undefined);
        await queryRunner.query(`DROP TABLE "intent_reference"`, undefined);
        await queryRunner.query(`DROP TABLE "intent"`, undefined);
    }

}
