import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import {INestApplication, Injectable} from "@nestjs/common";
import {getConnection, Repository} from "typeorm";
import {Bot} from "../src/models/bot.entity";
import {InjectRepository, TypeOrmModule} from "@nestjs/typeorm";

describe('Watson.V1.MessagesController (e2e)', () => {
  let app: INestApplication;

  @Injectable()
  class FixtureService {
    @InjectRepository(Bot)
    private readonly botRepository: Repository<Bot>;
    async createBot(): Promise<Bot> {
      const bot = new Bot();
      bot.name = "Testbot";
      return this.botRepository.save(bot);
    }
  }

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, TypeOrmModule.forFeature([Bot])],
      providers: [FixtureService]
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/watson/v1/:botId/messages (POST)', async () => {
    const fixtureService = app.get<FixtureService>(FixtureService);
    const bot = await fixtureService.createBot();
    return request(app.getHttpServer())
      .post(`/watson/v1/${bot.id}/messages`).send(
        {
            "context": {
              "_id": "6219ec1dce54ca23da813928989d5d95",
              "_rev": "1-43db3d3606c2e18a8fc25010b0e13559",
              "callAgent": "tel:+4917672171390",
              "conversation_id": "f75da79e-78b1-4115-a2de-6aa75a315e36",
              "defaultLanguage": "DE",
              "deviceNumber": "sip:+4917656965411@parkagent.pstn.twilio.com",
              "deviceType": "entry",
              "garageName": "Marcus Ilgner",
              "garageOperator": "Marcus Ilgner GmbH",
              "input": "bekomme ich will gerade ",
              "metadata": {
                "user_id": "ibm-system-ivr-user"
              },
              "status": "forwarded",
              "system": {
                "_node_output_map": {
                  "node_1_1554987694197": [
                    0,
                    1,
                    0
                  ],
                  "node_9_1568274562246": [
                    0
                  ],
                  "node_9_1568274745008": [
                    0
                  ]
                },
                "branch_exited": true,
                "branch_exited_reason": "completed",
                "dialog_request_counter": 3,
                "dialog_stack": [
                  {
                    "dialog_node": "root"
                  }
                ],
                "dialog_turn_counter": 3,
                "initialized": true
              },
              "vgwBargeInOccurred": "No",
              "vgwCompletedActions": [
                {
                  "command": "vgwActPlayText",
                  "parameters": {
                    "text": [
                      "Wie kann ich Ihnen weiterhelfen?"
                    ]
                  },
                  "processed": true
                }
              ],
              "vgwIsCaller": "Yes",
              "vgwIsDTMF": "No",
              "vgwSIPCallID": "101484747_100631964@10.186.123.10",
              "vgwSIPFromURI": "sip:+4917656965411@parkagent.pstn.twilio.com",
              "vgwSIPRequestURI": "sip:4932211001391@us-south.voiceagent.cloud.ibm.com:5060;transport=udp;region=ie1",
              "vgwSIPToURI": "sip:+4932211001391@us-south.voiceagent.cloud.ibm.com;transport=udp;region=ie1",
              "vgwSTTProvider": "Dallas",
              "vgwSTTResponse": {
                "config": {
                  "bargeInResume": false,
                  "confidenceScoreThreshold": 0,
                  "config": {
                    "firmup_silence_time": 1,
                    "model": "en-US_NarrowbandModel",
                    "smart_formatting": true
                  },
                  "connectionTimeout": 5,
                  "credentials": {
                    "password": null,
                    "tokenAuthEnabled": false,
                    "tokenServiceProviderUrl": "https://stream.watsonplatform.net/authorization/api/v1/token",
                    "tokenServiceUrl": "https://stream.watsonplatform.net/speech-to-text/api",
                    "url": "https://stream.watsonplatform.net/speech-to-text/api",
                    "username": null
                  },
                  "disableFirstTurnBargeIn": false,
                  "echoSuppression": false,
                  "enableRapidBargeIn": true,
                  "providerSelectionPolicy": "sequential",
                  "providers": [
                    {
                      "bargeInResume": true,
                      "broadbandConfig": {
                        "model": "de-DE_BroadbandModel",
                        "model_desc": "German broadband model",
                        "smart_formatting": true
                      },
                      "config": {
                        "firmup_silence_time": 1,
                        "model": "",
                        "model_desc": "",
                        "profanity_filter": true,
                        "smart_formatting": true
                      },
                      "credentials": {
                        "apikey": "*************",
                        "credential_name": "Credentials-1",
                        "tokenAuthEnabled": true,
                        "tokenServiceProviderUrl": "https://iam.cloud.ibm.com/identity/token",
                        "url": "https://stream.watsonplatform.net/speech-to-text/api"
                      },
                      "echoSuppression": true,
                      "enabled": true,
                      "instance_id": "crn:v1:bluemix:public:speech-to-text:us-south:a/086da536d911895b4bc37390af6a3bda:9a0f2de5-eb29-4f26-b9bf-598d281aea0a::",
                      "instance_name": "VoiceAgent-SpeechToText-2c8xbz",
                      "modelType": "Broadband",
                      "name": "Dallas",
                      "region": "us-south",
                      "thirdPartyCredentials": "*************",
                      "type": "my_stt"
                    }
                  ],
                  "requestTimeout": 5
                },
                "result_index": 0,
                "results": [
                  {
                    "alternatives": [
                      {
                        "confidence": 0.52,
                        "transcript": "bekomme ich will gerade "
                      }
                    ],
                    "final": true
                  }
                ]
              },
              "vgwSessionID": "6dae0047-0334-4830-b4ff-e4ab82989795",
              "vgwTTSProvider": "Dallas",
              "vgwTextAlternatives": "[{\"transcript\":\"bekomme ich will gerade \",\"confidence\":0.52}]",
              "vgwTranscriptionSource": "sip:+4917656965411@parkagent.pstn.twilio.com"
            },
            "entities": [
              {
                "confidence": 1,
                "entity": "sys-time",
                "location": [
                  17,
                  23
                ],
                "metadata": {
                  "calendar_type": "GREGORIAN",
                  "timezone": "GMT"
                },
                "value": "11:21:43"
              }
            ],
            "input": {
              "text": "bekomme ich will gerade "
            },
            "intents": [
              {
                "intent": "schalala",
                "confidence": "0.9"
              }
            ],
            "output": {
              "generic": [
                {
                  "response_type": "text",
                  "text": "Bitte warten Sie kurz, während ich Sie mit einem Servicemitarbeiter verbinde."
                }
              ],
              "log_messages": [],
              "nodes_visited": [
                "node_1_1568297927968",
                "node_1_1554987694197"
              ],
              "text": [
                "Bitte warten Sie kurz, während ich Sie mit einem Servicemitarbeiter verbinde."
              ],
              "vgwAction": {
                "command": "vgwActTransfer",
                "parameters": {
                  "transferTarget": "tel:+4917672171390"
                }
              }
            }
          }
      )
      .expect(204);
  });
});
